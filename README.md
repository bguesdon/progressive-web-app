# Progressive Wep App

## SERVER

### Installation
``` bash
$ npm install
$ npm start
$ open http://localhost:3000/api-docs
```

### Initialize database
Import `/server/database/init.sql` into database to initialize it.

### Environnement variables
You must create a `.env` file at the root of the server folder, e.g:
```
APP_ENV={dev|prod}

DB_HOST={your_db_host}
DB_USER={your_db_user}
DB_PASSWORD={your_db_password}
DB_NAME={your_db_name}

JWT_SECRET={your_jwt_secret}
```

## CLIENT


