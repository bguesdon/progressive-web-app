const express = require('express')
const app = express()
const https = require('https')
const fs = require('fs')
const helmet = require('helmet')
const path = require('path')
const bodyParser = require('body-parser')
const color = require('colors')
const session = require('express-session')
const fileUpload = require('express-fileupload')

const config = require('./config')
const swaggerUi = require('swagger-ui-express')
const swaggerDoc = require('./config/swagger/swagger.json')
const AuthenticationMiddleware = require('./src/services/AuthenticationMiddleware')
const NotificationService = require('./src/services/NotificationService')

// Configuration environnement variables
const result = require('dotenv').config()
if (result.error) {
    throw result.error
}

module.exports = {
    _initMiddleWares(app) {
        app.use((req, res, next) => {
            const origin = config.client.url

            res.setHeader("Access-Control-Allow-Origin", origin)
            res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
            if ('OPTIONS' === req.method) {
                return res
                    .status(200)
                    .json({ message: 'OK' })
            }
            
            next()
        })

        app.use('*', AuthenticationMiddleware)
        app.use(helmet())
    },

    _initRoutes(app, done) {
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))
        
        fs.readdir('./src/routes', (err, files) => {
            if (err) {
                return done(err.toString())
            }

            files.forEach(filename => {
                const file = './src/routes/' + filename
                const route = require(file)

                route.routes(app)
            })

            return done(null)
        })
    },

    _initNotificationPush(app) {
        NotificationService._init(app)
    },

    stop(done = () => { }) {
        this._server.close()
        done()
    },

    run(done) {
        app.use(bodyParser.urlencoded({ extended: true }))
        app.use(bodyParser.json({ limit: '50mb' }))
        app.use(fileUpload({
            createParentPath: true,
            safeFileNames: false,
        }))

        app.set('trust proxy', 1)

        app.use(session({
            secret: 'enoLlkdPceecnuDncNDNcndND',
            resave: true,
            saveUninitialized: true,
            cookie: { secure: true }
        }))

        this._initMiddleWares(app)
        this._initRoutes(app, (err) => {
            if (err) {
                return done(err)
            }
        })
        this._initNotificationPush(app)

        // Set global paths
        global.__basedir = __dirname
        global.__publicDir = path.join(__dirname, './public/')
        global.__imagesDir = path.join(__dirname, './public/images')

        global.express = app
        global.app = this

        app.use(express.static(global.__publicDir))

        if (process.env.NODE_ENV == 'production') {
            this._server = https.createServer({
                key: fs.readFileSync('/etc/letsencrypt/live/www.chittync.me/privkey.pem').toString(),
                cert: fs.readFileSync('/etc/letsencrypt/live/www.chittync.me/fullchain.pem').toString(),
                dhparam: fs.readFileSync('/etc/letsencrypt/live/www.chittync.me/dh-strong.pem').toString()
            }, app).listen(config.server.port, () => {
                console.log(`Listening on ${config.server.url}`.cyan)
                done()
            })
        } else {
            this._server = app.listen(config.server.port, () => {
                console.log(`Listening on ${config.server.url}`.cyan)
                done()
            })
        }
    }
}
