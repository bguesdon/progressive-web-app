const UserService = require('../../services/UserService')

module.exports = {
    getProfile(req, res) {
        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            UserService.getUserByUuid(uuid, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                return res
                    .status(200)
                    .json(result)
            })
        })
    },

    updateProfile(req, res) {
        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            UserService.getUserByUuid(uuid, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                const data = {
                    uuid: uuid,
                    username: req.body.username ? req.body.username : null,
                    email: req.body.email ? req.body.email : null,
                    password: req.body.password ? req.body.password : null
                }
    
                UserService.updateUser(data, (err, result) => {
                    if (err) {
                        return res
                            .status(err.status)
                            .json({
                                error: err.error
                            })
                    }
    
                    return res
                        .status(201)
                        .json({
                            message: 'Updated'
                        })
                })
            })
        })
    },

    routes(app) {
        app.get('/profile', this.getProfile)
        app.put('/profile', this.updateProfile)
    }
}