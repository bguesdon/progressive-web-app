const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const webpush = require('web-push')

const AuthenticationService = require('../../services/AuthenticationService')
const NotificationService = require('../../services/NotificationService')

module.exports = {
    login(req, res) {
        if (!req.body.username || !req.body.password) {
            return res
                .status(401)
                .json({
                    message: 'Bad credentials'
                })
        }

        AuthenticationService.generateJwt(req.user, (err, result) => {
            if (err || !result.token) {
                return res
                    .status(err.status)
                    .json({
                        error: err
                    })
            }

            // Push notification
            NotificationService.send(req.user.uuid, JSON.stringify({
                title: "Connecté !",
                body: "Vous êtes connecté"
            }))

            return res
                .status(200)
                .cookie('access-token', result.token, { maxAge: 60 * 60 * 24 * 1000 })
                .json({
                    message: 'OK',
                    'access-token': result.token
                })

        })
    },

    logout(req, res) {
        req.logout()

        return res
            .status(200)
            .json({
                message: 'OK'
            })
    },

    register(req, res) {
        if (!req.body.email || !req.body.username || !req.body.password) {
            return res
                .status(400)
                .json({
                    message: 'Missing credentials'
                })
        }

        AuthenticationService.register(req.body, (err) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            return res
                .status(201)
                .json({
                    message: 'Created'
                })
        })
    },

    routes(app) {
        AuthenticationService._init(app)
        app.post('/auth/login', passport.authenticate('local'), this.login)
        app.post('/auth/logout', this.logout)
        app.post('/auth/register', this.register)
    }
}