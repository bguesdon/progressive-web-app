const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const saltRounds = 10

const db = require('./DatabaseService')
const config = require('../../config')

module.exports = {
    getUsers(done) {
        db.query('SELECT uuid, username, email FROM users', (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null, result)
        })
    },

    getUserByUuid(uuid, done) {
        db.query('SELECT uuid, username, email FROM users WHERE uuid = ?', [uuid], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }
            if (result.length == 0) {
                return done({
                    status: 404,
                    error: 'User not found'
                })
            }

            return done(null, result[0])
        })
    },

    deleteUser(uuid, done) {
        db.query('DELETE from users WHERE uuid = ?', [uuid], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null)
        })
    },

    updateUser(data, done) {
        if (data.password) {
            data.password = bcrypt.hashSync(data.password, saltRounds)
        }

        let sql = ''
        for (let i in data) {
            if (data[i] && i != 'uuid') {
                if (sql.length == 0) {
                    sql += `${i} = '${data[i]}'`
                } else {
                    sql += `, ${i} = '${data[i]}'`
                }
            }
        }

        db.query(`UPDATE users SET ${sql} WHERE uuid = ?`, [ data.uuid ], (err, result) => {
            if (err) {
                console.error(err)
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null, result)
        })
    },

    getUserUuid(req, done) {
        if (!req.headers.authorization) {
            return done({
                status: 401,
                error: 'No token provided'
            })
        }

        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            return done({
                status: 401,
                error: 'No token provided'
            })
        }

        jwt.verify(token, config.jwt.secret, (err, decoded) => {
            if (err || !decoded.uuid) {
                return done({
                    status: 500,
                    error: 'Invalid token'
                })
            }

            return done(null, decoded.uuid)
        })
    }
}