{
	"openapi": "3.0.3",
	"info": {
		"version": "1.0.0",
		"title": "Progressive Web App - API",
		"description": "These are the routes available on the API",
		"contact": {
			"name": "Tom Chitty",
			"email": "tom.chitty@epitech.eu"
		},
		"license": {
			"name": "Apache 2.0",
			"url": "https://www.apache.org/licenses/LICENSE-2.0.html"
		}
	},
	"servers": [
		{ "url": "http://localhost:3000" },
		{ "url": "http://51.83.45.42:3000" }
	],
	"tags": [
		{
			"name": "Authentication",
			"description": "API authentication routes"
		},
		{
				"name": "Users",
				"description": "API users routes"
		},
		{
			"name": "Profile",
			"description": "API user profile routes"
		},
		{
			"name": "Images",
			"description": "API images routes"
		},
		{
			"name": "Friends",
			"description": "API user friends routes"
		}
	],
	"schemes": [
		"http",
		"https"
	],
	"paths": {
		"/auth/login": {
			"post": {
				"tags": ["Authentication"],
				"summary": "Login a user",
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "User credentials",
					"required": true,
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/requestBodies/Login"
							}
						}
					}
				},
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/responses/Login"
								}
							}
						}
					},
					"401": {
						"description": "Bad credentials"
					}
				}
			}
		},
		"/auth/register": {
			"post": {
				"tags": ["Authentication"],
				"summary": "Register a user",
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "User credentials",
					"required": true,
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/requestBodies/Register"
							}
						}
					}
				},
				"responses": {
					"201": {
						"description": "Created"
					},
					"409": {
						"description": "E-mail address already used"
					}
				}
			}
		},
		"/auth/logout": {
			"post": {
				"tags": ["Authentication"],
				"summary": "Logout a user",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"responses": {
					"200": {
						"description": "OK"
					}
				}
			}
		},

		"/users": {
			"get": {
				"tags": ["Users"],
				"summary": "Get all users",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"responses": {
					"200": {
						"description": "List of users",
						"content": {
							"application/json": {
								"schema": {
									"type": "array",
									"items": {
										"$ref": "#/components/schemas/User"
									}
								}
							}
						}
					}
				}
			}
		},
		"/users/{uuid}": {
			"get": {
				"tags": ["Users"],
				"summary": "Get user by uuid",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the user",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/User"
								}
							}
						}
					},
					"404": {
						"description": "User not found"
					}
				}
			}
		},
		"/users/me": {
			"delete": {
				"tags": ["Users"],
				"summary": "Delete current user",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"responses": {
					"200": {
						"description": "OK"
					}
				}
			}
		},

		"/profile": {
			"get": {
				"tags": ["Profile"],
				"summary": "Get user profile information",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/User"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			},
			"put": {
				"tags": ["Profile"],
				"summary": "Update user profile information",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "User information",
					"required": true,
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/requestBodies/User"
							}
						}
					}
				},
				"responses": {
					"201": {
						"description": "Created"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},

		"/images": {
			"get": {
				"tags": ["Images"],
				"summary": "Get user images",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"type": "array",
									"items":{
										"$ref": "#/components/schemas/Image"
									}
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			},
			"post": {
				"tags": ["Images"],
				"summary": "Upload user image",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "Image information",
					"required": true,
					"content": {
						"multipart/form-data": {
							"schema": {
								"$ref": "#/components/requestBodies/ImageCreation"
							}
						}
					}
				},
				"responses": {
					"201": {
						"description": "Created"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},
		"/images-base64": {
			"post": {
				"tags": ["Images"],
				"summary": "Upload user image in base64",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "Image information",
					"required": true,
					"content": {
						"multipart/form-data": {
							"schema": {
								"$ref": "#/components/requestBodies/ImageCreationBase64"
							}
						}
					}
				},
				"responses": {
					"201": {
						"description": "Created"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},
		"/images/{uuid}": {
			"get": {
				"tags": ["Images"],
				"summary": "Get user image",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the image",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/Image"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					},
					"404": {
						"description": "Image not found"
					}
				}
			},
			"put": {
				"tags": ["Images"],
				"summary": "Update user image",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "Image information",
					"required": true,
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/requestBodies/ImageEdition"
							}
						}
					}
				},
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the image",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"201": {
						"description": "Edited"
					},
					"401": {
						"description": "Unauthorized"
					},
					"404": {
						"description": "Image not found"
					}
				}
			},
			"delete": {
				"tags": ["Images"],
				"summary": "Delete user image",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the image",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK"
					},
					"401": {
						"description": "Unauthorized"
					},
					"404": {
						"description": "Image not found"
					}
				}
			}
		},

		"/friends": {
			"get": {
				"tags": ["Friends"],
				"summary": "Get user friends",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"produces": ["application/json"],
				"parameters": [
					{
						"name": "status",
						"in": "query",
						"description": "Status of the relationship (pending, accepted, declined)",
						"require": false,
						"type": "string"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"type": "array",
									"items": {
										"$ref": "#/components/responses/FriendRelationship"
									}
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},
		"/friends/{uuid}": {
			"get": {
				"tags": ["Friends"],
				"summary": "Get friend information",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the requested user",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/User"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			},
			"post": {
				"tags": ["Friends"],
				"summary": "Send request to user",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the requested user",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"201": {
						"description": "Created"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			},
			"put": {
				"tags": ["Friends"],
				"summary": "Edit status of relationship",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"consumes": ["application/json"],
				"requestBody": {
					"description": "Relationship status (pending, accepted, declined)",
					"required": true,
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/requestBodies/FriendRelationshipEdition"
							}
						}
					}
				},
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the friend to edit",
						"required": true,
						"type": "string",
						"format": "uuid"
					}
				],
				"responses": {
					"201": {
						"description": "Edited"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			},
			"delete": {
				"tags": ["Friends"],
				"summary": "Remove friend relationship",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the friend to remove",
						"required": true,
						"type": "string",
						"format" :"uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK"
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},
		"/friends/{uuid}/images": {
			"get": {
				"tags": ["Friends"],
				"summary": "Get friend images",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the user friend",
						"required": true,
						"type": "string",
						"format" :"uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"type": "array",
									"items": {
										"$ref": "#/components/schemas/Image"
									}
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
			}
		},
		"/friends/{uuid}/images/{imgId}": {
			"get": {
				"tags": ["Friends"],
				"summary": "Get friend images",
				"security": [
					{
						"BearerAuth": []
					}
				],
				"description": "",
				"parameters": [
					{
						"name": "uuid",
						"in": "path",
						"description": "Uuid of the user friend",
						"required": true,
						"type": "string",
						"format" :"uuid"
					},
					{
						"name": "imgId",
						"in": "path",
						"description": "Uuid of the image requested",
						"required": true,
						"type": "string",
						"format" :"uuid"
					}
				],
				"responses": {
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/Image"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized"
					}
				}
				
			}
		}
	},

	"components": {
		"schemas": {
			"User": {
				"type": "object",
				"properties": {
					"uuid": {
						"type": "string",
						"format": "uuid",
						"readOnly": true
					},
					"username": {
						"type": "string"
					},
					"email": {
						"type": "string"
					},
					"password": {
						"type": "string",
						"format": "password",
						"writeOnly": true
					}
				}
			},

			"Image": {
				"type": "object",
				"properties": {
					"uuid": {
						"type": "string",
						"format": "uuid",
						"readOnly": true
					},
					"title": {
						"type": "string"
					},
					"description": {
						"type": "string"
					},
					"image": {
						"type": "string",
						"format": "binary"
					},
					"createAt": {
						"type": "string",
						"format": "date-time"
					},
					"author": {
						"type": "string",
						"format": "uuid"
					}
				}
			},

			"FriendRelationship": {
				"type": "object",
				"properties": {
					"fromUser": {
						"type": "string",
						"format": "uuid"
					},
					"toUser": {
						"type": "string",
						"format": "uuid"
					},
					"status": {
						"type": "string"
					}
				}
			}
		},
		"requestBodies": {
			"User": {
				"type": "object",
				"properties": {
					"username": {
						"type": "string",
						"required": true
					},
					"email": {
						"type": "string",
						"format": "email"
					},
					"password": {
						"type": "string",
						"format": "password",
						"writeOnly": true
					}
				}
			},
			"Login": {
				"type": "object",
				"properties": {
					"username": {
						"type": "string"
					},
					"password": {
						"type": "string",
						"format": "password"
					}
				}
			},
			"Register": {
				"type": "object",
				"properties": {
					"email": {
						"type": "string",
						"format": "email"
					},
					"username": {
						"type": "string"
					},
					"password": {
						"type": "string",
						"format": "password"
					}
				}
			},
			"ImageCreation": {
				"type": "object",
				"properties": {
					"title": {
						"type": "string"
					},
					"description": {
						"type": "string"
					},
					"file": {
						"type": "string",
						"format": "base64"
					}
				}
			},
			"ImageCreationBase64": {
				"type": "object",
				"properties": {
					"title": {
						"type": "string"
					},
					"description": {
						"type": "string"
					},
					"file": {
						"type": "string"
					}
				}
			},
			"ImageEdition": {
				"type": "object",
				"properties": {
					"title": {
						"type": "string"
					},
					"description": {
						"type": "string"
					}
				}
			},
			"FriendRelationshipEdition": {
				"type": "object",
				"properties": {
					"status": {
						"type": "string"
					}
				}
			}
		},
		"responses": {
			"Login": {
				"type": "object",
				"properties": {
					"access-token": {
						"type": "string"
					},
					"refresh-token": {
						"type": "string"
					}
				}
			},

			"FriendRelationship": {
				"type": "object",
				"properties": {
					"friend": {
						"$ref": "#/components/schemas/User"
					},
					"status": {
						"type": "string"
					}
				}
			}
		},
		"securitySchemes": {
			"BearerAuth": {
				"type": "http",
				"scheme": "bearer"
			}
		}
	},

	"security": [
		{
			"BearerAuth": []
		}
	]
}
