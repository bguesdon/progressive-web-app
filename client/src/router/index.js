import Vue from "vue";
import Router from "vue-router";
import routes from "./routes";
import store from "@/store";

Vue.use(Router);

const router = new Router(routes);

router.beforeEach((to, from, next) => {
  const authRequired = authentificationRequired(to);
  const isUserLogged = store.getters["user/isLogedin"];

  if (authRequired) {
    if (isUserLogged) {
      next();
    } else {
      next("/login");
    }
  } else if (to.name === "login" && isUserLogged) {
    next('/home');
  } else {
    next();
  }
});

function authentificationRequired(to) {
  return to.matched.some(record => record.meta.requiresAuth);
}


export default router;
