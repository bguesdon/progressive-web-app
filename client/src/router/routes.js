import Home from "@/pages/app/Home.vue";
import Login from "@/pages/app/Login.vue";
import Register from "@/pages/app/Register";
import Camera from "@/components/Camera/Camera";
import Profile from "@/pages/app/Profile";
import RequestList from "@/pages/app/RequestList";

export default {
  routes: [
    {
      path: "/camera",
      name: "camera",
      component: Camera,
      meta: { requiresAuth: true }
    },
    {
      path: "/request",
      name: "request",
      component: RequestList,
      meta: {requiresAuth: true}
    },
    {
      path: "/profil/:userId",
      name: "profil",
      component: Profile,
      meta: { requiresAuth: true}
    },
    {
      path: "/",
      name: "home",
      component: Home,
      meta: { requiresAuth: true }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: { requiresAuth: false , fullWidth: true }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: { requiresAuth: false , fullWidth: true }
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
};
