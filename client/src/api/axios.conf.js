import axios from "axios";
import store from "@/store";
import vueInstance from "@/main";
import router from "@/router";

export function getAxios(params) {
  let headers = base_header();

  checkToken(headers);

  const instance = axios.create({
    baseURL: process.env.VUE_APP_WS_ENDPOINT,
    headers: headers
  });

  instance.interceptors.response.use(
    success(),
    error()
  );
  return instance;
}

function base_header() {
  return {
    accept: "application/json",
    "Content-Type": "application/json",
  };
}

function checkToken(headers) {
  const token = store.getters["user/token"];
  if (token) {
    headers.Authorization = "Bearer " + token;
  }
}


function success() {
  return response => {
    if (response.status){
     if ([204].includes(response.status)) {
       throw "204"
      }
    }
    return response;
  };
}

function error() {
  return error => {
    if (
      typeof error.response !== "undefined" &&
      typeof error.response.status !== "undefined"
    ) {
      httpCodeErrorHandler(error.response.status, error.response.data.message);
    }

    return Promise.reject(error);
  };
}

function httpCodeErrorHandler(status, message) {
  if ([401].includes(status)) {
  } else if ([403].includes(status)) {
    console.log("403");
  } else if ([500, 503, 504].includes(status)) {
    console.log("500, 503, 504");
  } else if ([400, 404].includes(status)) {
    console.log("400, 404");
  }
}
