import { getAxios } from "./axios.conf.js";


export function registerToApi() {
        Notification.requestPermission(function(status) {
            if (status == 'granted') {
                callApi()
            }
          })
}

async function callApi() {
    const sw = await navigator.serviceWorker.ready
    const push = await sw.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: 'BD-53NyeNKPl0NVSR4lQK9G3ItE8PlXhPhuD7-M7E4UybfskvBEDfW5o6gMnvsNdpDj9MKCsv_iyKtllmiSi_oo'
    })

    getAxios().post('/subscribe', push)
}