import { getAxios } from "./axios.conf.js";

export function login(payload) {
    return getAxios().post(`/auth/login`, payload)
}

export function getProfile() {
    return getAxios().get(`/profile`);
}

export function logout() {
    return getAxios().post(`/auth/logout`);
}

export function register(payload) {
    return getAxios().post(`/auth/register`, payload);
}

export function getUser(userId) {
    return getAxios().get(
        `/users/${userId}`
    );
}

export function getUsers(paylaod) {
    return getAxios().get(`/users`, paylaod);
}

export function uploadImage(payload) {
    return getAxios().post('/images-base64', payload);
}

export function getCurrentUserImages() {
    return getAxios().get('/images');
}

export function addFriend(userId) {
    return getAxios().post(
        `/friends/${userId}`);
}

export function getFriendList(payload) {
    return getAxios().get('/friends', payload);
}


export function acceptFriend(userId) {
    return getAxios().put(
        `/friends/${userId}`, {status: "accepted"});
}

export function refuseFriend(userId) {
    return getAxios().put(
        `/friends/${userId}`, {status: "declined"});
}

export function getFriendImage(userId) {
    return getAxios().get(
        `/friends/${userId}/images`);
}

export function getFriendImages() {
    return getAxios().get(
        `/friends/last-images`);
}
