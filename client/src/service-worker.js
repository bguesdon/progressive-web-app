// /**
//  * Welcome to your Workbox-powered service worker!
//  *
//  * You'll need to register this file in your web app and you should
//  * disable HTTP caching for this file too.
//  * See https://goo.gl/nhQhGp
//  *
//  * The rest of the code is auto-generated. Please don't update this file
//  * directly; instead, make changes to your Workbox build configuration
//  * and re-run your build process.
//  * See https://goo.gl/2aRDsh
//  */

// importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

// importScripts(
//   "/precache-manifest.dc46290ce6460b737cafec54e23c0d63.js"
// );

// workbox.core.setCacheNameDetails({prefix: "Pic'Share"});

// self.addEventListener('message', (event) => {
//   if (event.data && event.data.type === 'SKIP_WAITING') {
//     self.skipWaiting();
//   }
// });

// /**
//  * The workboxSW.precacheAndRoute() method efficiently caches and responds to
//  * requests for URLs in the manifest.
//  * See https://goo.gl/S9QRab
//  */
// self.__precacheManifest = [].concat(self.__precacheManifest || []);
// workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

self.__precacheManifest = [].concat(self.__precacheManifest || []);

workbox.setConfig({
  debug: true
});

workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

let click_open_url;

self.addEventListener("push", function(event) {
  let push_message = event.data.json();

  console.log('push_message.click_open_url', push_message.click_open_url)

  click_open_url = push_message.click_open_url ? push_message.click_open_url : null
  const options = {
    body: push_message.body,
    icon: "./img/icons/favicon-32x32.png",
    vibrate: [200, 100, 200, 100, 200, 100, 200],
    tag: "vibration-sample"
  };
  event.waitUntil(
    self.registration.showNotification(push_message.title, options)
  );
});

self.addEventListener("notificationclick", function(event) {
  const clickedNotification = event.notification;
  clickedNotification.close();
  console.log('click_open_url', click_open_url)
  if (click_open_url) {
    const promiseChain = clients.openWindow(click_open_url);
    event.waitUntil(promiseChain);
  }
});