module.exports = {
  important: false,
  theme: {
    gap: {
      // defaults to {}
      "0": "0",
      "1": "0.25rem",
      "2": "0.5rem",
      "3": "0.75rem",
      "4": "1rem",
      "5": "1.25rem",
      "6": "1.5rem",
      "7": "1.75rem",
      "8": "2rem"
    },
    linearGradients: {
      directions: {
        // defaults to these values
        t: "to top",
        tr: "to top right",
        r: "to right",
        br: "to bottom right",
        b: "to bottom",
        bl: "to bottom left",
        l: "to left",
        tl: "to top left"
      },
      colors: {
        // defaults to {}
        "ibisa-sunset": ["#ee0979", "#ff6a00"],
        scooter: ["#36D1DC 0%", "#5B86E5"],
        "scooter-darker": ["#36D1DC 0%", "#5B86E5"],
        "black-white-with-stops": ["#000", "#000 45%", "#fff 55%", "#fff"]
      }
    },
    extend: {
      colors: {
        primary: "linear-gradient(-90deg, red, yellow)",
        secondary: "#ecc94b"
      },
      screens: {
        dark: { raw: "(prefers-color-scheme: dark)" }
      },
      spacing: {
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
        '108': '27rem',
        '120': '30rem',
      }
    }
  },
  variants: {
    borderColor: ["responsive", "hover", "focus", "focus-within"],
    borderWidth: ["responsive", "last"],
    backgroundColor: ["responsive", "hover", "focus", "odd", "disabled", "active"],
    opacity: ["responsive", "hover", "focus", "disabled"]
  },
  plugins: [
    require("tailwindcss-gradients")(),
    require("tailwindcss-gap")({
      legacy: false // defaults to false, set to true to output IE-compatible CSS (no custom properties, but much larger CSS for the same functionality)
    })
  ]
};
