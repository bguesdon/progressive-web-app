import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faUser,
    faAngleUp,
    faAngleDown,
    faHome,
    faPaperPlane,
    faTimes,
    faCamera,
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faUser,
    faAngleUp,
    faAngleDown,
    faHome,
    faPaperPlane,
    faTimes,
    faCamera
);
