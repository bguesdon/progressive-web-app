import Vue from "vue";
import VeeValidate from "vee-validate";

const dictionary = {
  fr: {
    messages: {
      required: () => `Le champs est requis`,
      regex: (fieldName, args) => `Le champs doit respecter la forme ${args}`
    }
  }
};

Vue.use(VeeValidate);
VeeValidate.Validator.localize(dictionary);
VeeValidate.Validator.localize("fr");
