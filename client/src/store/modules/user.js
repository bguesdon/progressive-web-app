import {getFriendList, getProfile} from "@/api";


export default {
  namespaced: true,
  state: {
    username: null,
    token: null,
    refreshToken: null,
    id: null,
    email: null,
    allUsers: null,
    friendList: null,
  },
  mutations: {
    SET_USER: (state, payload) => {
      state.username = payload.username;
      state.id = payload.uuid;
      state.email = payload.email;
    },
    SET_TOKEN: (state, payload) => {
      state.token = payload["access-token"];
      state.refreshToken = payload["refresh-token"]
    },
    CLEAN_STATE: state => {
      state.username = null;
      state.refreshToken = null;
      state.token = null;
      state.id = null;
      state.email = null;
      state.friendList = null;
    },
    SET_USERS: (state, payload) => {
      state.allUsers = payload;
    },
    SET_FRIENDS: (state, payload) => {
        state.friendList = payload;
    }
  },
  actions: {
    getUser: async store => {
      return getProfile().then(({ data }) =>{
        store.commit("SET_USER", data);
        return data;
      })
    },
    getUserFriendList: async store => {
        await getFriendList().then(res => {
          store.commit('SET_FRIENDS', res.data);
        });
          return store.getters.friendList;
    }
  },
  getters: {
    isLogedin: state => state.token != null,
    token: state => state.token,
    username: state => state.username,
    email: state => state.email,
    allUsers: state => state.allUsers,
    userId: state => state.id,
    friendList: state => state.friendList,
  }
};
