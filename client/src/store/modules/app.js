import {getUsers, login, logout} from "@/api";
import { registerToApi } from "@/api/notifications";

export let mutations = {

};

export let actions = {
  logout: store => {
      return logout().then(async resp => {
          store.commit("user/CLEAN_STATE", null, { root: true });
          return Promise.resolve(resp.data);
      })
          .catch(function(error) {
              return Promise.reject(error);
          });
  },

  login: (store, payload) => {
    return login(
      {
        username: payload.username,
        password: payload.password
      }
    )
      .then(async resp => {
        store.commit("user/SET_TOKEN", resp.data, { root: true });
        await store.dispatch("user/getUser", {}, { root: true });
        registerToApi();
        getUsers().then( async resp => {
            store.commit('user/SET_USERS', resp.data, {root: true});
        });
        await store.dispatch("user/getUserFriendList", {}, {root: true});
        return Promise.resolve(resp.data);
      })
      .catch(function(error) {
        return Promise.reject(error);
      });
  },

};

export let getters = {};

export let app = {
    name: "app",
    namespaced: true,
    state: {},
    mutations: mutations,
    actions: actions,
    getters: getters,
};
