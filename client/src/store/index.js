import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import user from "./modules/user";
import {app} from './modules/app'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user: user,
    app: app
  },
  plugins: [
    createPersistedState({
      key: "Pic'Share",
      paths: ["user"]
    })
  ]
});
