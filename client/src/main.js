import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/assets/styles/tailwind.scss";
import "./assets/icons/fontAwesome";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "@/conf/veeValidate";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.component("font-awesome-icon", FontAwesomeIcon);

export default new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
