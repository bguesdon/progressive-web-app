module.exports = {
    pwa: {
        name: "Pic'Share",
        workboxPluginMode: "InjectManifest",
        workboxOptions: {
            swSrc: "src/service-worker.js"
        }
    }
}