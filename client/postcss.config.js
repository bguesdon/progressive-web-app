
module.exports = {
  plugins: [
    require("tailwindcss")("./src/assets/styles/tailwind.js"),
    require("autoprefixer")()
  ]
};
